# Testing APIs

Using the ExchangeRate-API, you can see the [results](https://api.exchangerate-api.com/v4/latest/USD) from their API in your web browser. How's that possible?

This API method—often called an **API endpoint**—responds to a `GET` request, which is what your browser uses by default when you enter a URL, so their API returns the requested data.

Feel free to try their API with different currencies. Replace `USD` with `EUR` or `NZD`. You can find a list of their supported currencies at [Supported Currencies](https://www.exchangerate-api.com/docs/supported-currencies).

## Postman

Testing an API in a web browser doesn't work for API endpoints that require specific headers, need complex data parameters, or only respond to request methods other than `GET`.

However, there are better ways to test an API. One of the most widely used tools is [Postman](https://www.postman.com/). It's free to download and install on your computer.

The Intro to Tools: Postman unit can help you learn the Postman interface and how to create different types of requests.

### Using Postman to test ExchangeRate-API

Click the **Create a request** button under the "Get started" header on the right side of the window. You can also click the **+** button on the tab bar:

![Postman create request](./img/postman-create-request.png)

In the new tab that appears, enter `https://api.exchangerate-api.com/v4/latest/USD` in the textbox that reads "Enter request URL":

![Postman request](./img/postman-request-before.png)

After you click the **Send** button, you'll see the response appear in the lower half of the window. It's the same JSON that you saw when you put that address in your web browser:

![Postman response](./img/postman-request-after.png)

Postman displays the status code along with the response. This response is `200 OK` which is a successful response:

![Postman response status](./img/postman-response-status.png)

You can also view the response headers:

![Postman response headers](./img/postman-response-headers.png)

Postman offers several great features such as changing the HTTP method, like `POST` and `PUT`, and setting parameters, headers, and body values for sending data. You'll see more about these features in a later unit.
