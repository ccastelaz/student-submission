# Using an API in code

Using RestSharp, you can make a basic HTTP `GET` request using the following code:

```csharp
// Create a new client
RestClient client = new RestClient();

// define URL for API
RestRequest request = new RestRequest("https://api.exchangerate-api.com/v4/latest/USD"); 

// Make GET request using the client
IRestResponse response = client.Get(request);

// Your return data is in response.Content
Console.WriteLine(response.Content);
```

### !callout-info

## Note: What does `response.Content` contain?

`response.Content` is only a string representation of the JSON, just like if you saw the API response in your browser. In the next section, you'll see how RestSharp can automatically convert the response data into a C# object.

### !end-callout

## Specifying a base URL

Often your client code calls many different endpoints on the same API server. In this case, it's convenient to specify the URL of the server once, then add the _relative_ address to the endpoint when creating the request. RestSharp allows you to pass a `baseUrl` when you create the `RestClient` to accomplish this:

```csharp
// Create a new client
RestClient client = new RestClient("http://locahost:3000");

// Get all locations
RestRequest requestAll = new RestRequest("locations");
IRestResponse response = client.Get(requestAll);

// ...

// Get a single location
RestRequest requestSingle = new RestRequest("locations/2");
response = client.Get(requestSingle);
```
