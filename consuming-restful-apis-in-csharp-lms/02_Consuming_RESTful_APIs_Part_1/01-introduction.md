# Introduction

So far you've learned about the HTTP protocol and how requests are made. Now you'll learn how to make HTTP requests in your code and how to handle the data you get back.

To interact with web services, you need a way to send HTTP requests and process the results. This is usually accomplished using a class or library that provides a "client" to make the requests and methods to read the response from the server.

Throughout this unit, you’ll use the [RestSharp](https://restsharp.dev/) library to make API requests. The RestSharp library is added to projects separately as a package, as it's not included with the .NET Core framework. To add packages to .NET projects, you use Microsoft's package manager, NuGet. The code that you'll work with has RestSharp installed already.
