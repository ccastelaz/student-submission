# Processing API responses

Now you have the response, but how do you use the JSON data in your application?

## Response status

Before you process the response, you must check for a successful response. If you don't check for a successful response, your application may throw an error, or your user may wonder why nothing is happening.

As mentioned in the previous unit, HTTP responses include a status code. Anything in the 200-299 range is a success response. However, when performing a `GET` request, the only response code you need for success is `200`.

### !callout-info

## Note: Why only 200?

Different response codes mean different things, even if they're all in the "success" category. Some response codes are only suitable for request methods other than `GET`. You'll see an example of that in the next unit.

### !end-callout

The RestSharp library provides a property to determine if a response was successful: `response.IsSuccessful`. This property is a boolean that returns `true` if the response status code is 200-299:

```csharp
if (response.IsSuccessful)
{
    Console.WriteLine("Success!");
    Console.WriteLine(response.Content);
}
```

If you need to get the exact status code, there's a property called `response.StatusCode`. By default, this property returns the description of the status code, such as `OK` or `NotFound`. However, if you cast the property to an `int`, it returns the numerical status code.

```csharp
Console.WriteLine(response.StatusCode);
// Output: OK

Console.WriteLine((int)response.StatusCode);
// Output: 200
```

This is possible because the `StatusCode` property isn't a `string` or an `int`. It's a special type called `enum`. It's a way of providing string constants that are tied to a number. When you cast an `enum` string to an `int`, you get its underlying number. If you want to read more about `enum`s, you can read the [MSDN Documentation on the Enum Class](https://docs.microsoft.com/en-us/dotnet/api/system.enum#examples). There's also a page for [MSDN Documentation on the HttpStatusCode Enum](https://docs.microsoft.com/en-us/dotnet/api/system.net.httpstatuscode).

## Parsing JSON

RestSharp provides a built-in **deserializer**, which converts a string into an object or array of objects. RestSharp attempts to map every field in the JSON response to a C# class you provide. If the result is an array of objects, it attempts to create a structure you define. You could use a basic array or one of the C# collection classes, such as `List` or `Dictionary`, to store the data.

First, you need to define the class for JSON you're expecting. Take a look at the return JSON from the ExchangeRate-API:

```json
{
  "base": "USD",
  "date": "2021-08-10",
  "time_last_updated": 1628553601,
  "rates": {
    "USD": 1,
    "AED": 3.67,
    "AFN": 79.94,
    "ALL": 103.28,
    "AMD": 491.29,
    "ANG": 1.79,
    "AOA": 643.02,
    "ARS": 96.89,
    "AUD": 1.36,
    "AWG": 1.79,
    //the rest omitted for brevity
  }
}
```

You define your class with the same names:

```csharp
public class ExchangeRates
{
    public string Base { get; set; }
    public string Date { get; set; }
    public string TimeLastUpdated { get; set; }
    public Dictionary<string, double> Rates { get; set; }
}
```

Note the different casing of the field names in the JSON and the C# class properties, as well as the JSON field `"time_last_updated"` being mapped to `TimeLastUpdated`.

To map the fields, RestSharp tries different variations on names: converting to CamelCase, lowercase, adding, and removing underscores. RestSharp can also map array objects, like `"rates"`, into `List` and `Dictionary` types.

To have RestSharp automatically deserialize the data, you need to make a small change to the code you saw earlier:

```csharp
IRestResponse<ExchangeRates> response = client.Get<ExchangeRates>(request);
```

You've seen this syntax before when working with collections—for example `List<string>`. By using the type parameter to declare `IRestResponse` and calling `.Get()`, RestSharp attempts to map the response to the `ExchangeRates` class.

If the mapping is successful, your object is in the `response.Data` property instead of `response.Content`:

```csharp
ExchangeRates exRates = response.Data;
/*
    exRates.Base: "USD"
    exRates.Date: "2021-08-10"
    exRates.TimeLastUpdated: "1628553601"
    exRates.Rates:
        [0]: {[USD, 1]}
        [1]: {[AED, 3.67]}
        [2]: {[AFN, 79.94]}
        [3]: {[ALL, 103.28]}
        [4]: {[AMD, 491.29]}
        [5]: {[ANG, 1.79]}
        ...
*/
```

### !callout-info

## Note: `response.Content` is still available

The string representation of the data is still available in `response.Content` even if you use the type parameter.

### !end-callout

If you don't want all the fields returned by the API, you can omit them from your class. For example, if you didn't want the `base` and `date` fields, you could declare your class like this:

```csharp
public class ExchangeRates
{
    public string TimeLastUpdated { get; set; }
    public Dictionary<string, double> Rates { get; set; }
}
```

The `exRates` object would contain:

```csharp
ExchangeRates exRates = response.Data;
/*
    exRates.TimeLastUpdated: "1628553601"
    exRates.Rates:
        [0]: {[USD, 1]}
        [1]: {[AED, 3.67]}
        [2]: {[AFN, 79.94]}
        [3]: {[ALL, 103.28]}
        [4]: {[AMD, 491.29]}
        [5]: {[ANG, 1.79]}
        ...
*/
```
