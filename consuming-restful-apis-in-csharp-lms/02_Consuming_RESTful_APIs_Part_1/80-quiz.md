# Quiz
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: aa99c9b9-130e-44c6-8666-62dca305dd7d
* title: Consuming RESTful APIs (Part 1) 1
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

What does it mean to "consume an API?"

##### !end-question

##### !options

* Use an API too much
* Register for an account to use an API
* Interact with an API in code
* Include the RestSharp library in your C# project

##### !end-options

##### !answer

* Interact with an API in code

##### !end-answer

##### !explanation-correct

Interacting with an API, typically using a REST client like RestSharp's `RestClient`, is called "consuming an API."

##### !end-explanation

##### !explanation:Use an API too much

If you use an API too much, your requests may be limited, but that's not "consuming."

##### !end-explanation

##### !explanation:Register for an account to use an API

Some APIs may require you to register an account, but that's not "consuming."

##### !end-explanation

##### !explanation:Include the RestSharp library in your C# project

Including a REST client in your project enables your code to consume an API, but just including it isn't "consuming an API."

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: checkbox
* id: a92b3162-2293-4fff-932d-9998928234d0
* title: Consuming RESTful APIs (Part 1) 2
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

What does a deserializer do?

##### !end-question

##### !options

* Sends an HTTP request
* Creates an object
* Replaces the JSON string with a modified JSON string
* Sets the values of properties with matching names

##### !end-options

##### !answer

* Creates an object
* Sets the values of properties with matching names

##### !end-answer

##### !explanation-correct

A deserializer converts a JSON string into an object. It creates the new object and sets the values of its properties with names that correspond to properties in the JSON.

##### !end-explanation

##### !explanation:Sends an HTTP request

An HTTP request is often sent to retrieve the JSON string that's passed to the deserializer, but that's not part of what the deserializer does.

##### !end-explanation

##### !explanation:Replaces the JSON string with a modified JSON string

The deserializer doesn't replace or modify the JSON string.

##### !end-explanation

##### !explanation:Creates an object

That's one thing the deserializer does, but it does more than that.

##### !end-explanation

##### !explanation:Sets the values of properties with matching names

That's one thing the deserializer does, but it does more than that.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: d3f02625-2798-4fdb-b088-8073562d36f6
* title: Consuming RESTful APIs (Part 1) 3
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

How would you determine that `RestClient` received a response status code outside the 200-299 range?

##### !end-question

##### !options

* `RestClient` throws an exception
* `Get()` returns null
* The `IsSuccessful` property of the returned object is false
* Deserializing the JSON fails

##### !end-options

##### !answer

* The `IsSuccessful` property of the returned object is false

##### !end-answer

##### !explanation-correct

If a request isn't successful, the returned object (which implements the `IRestResponse` interface) has its `IsSuccessful` property set to false and its `StatusCode` property set to the status code was received.

##### !end-explanation

##### !explanation:`Get()` returns null

`Get()` always returns an object that implements the `IRestResponse` interface.

##### !end-explanation

##### !explanation:`RestClient` throws an exception

`RestClient` doesn't throw exceptions due to the response status code.

##### !end-explanation

##### !explanation:Deserializing the JSON fails

The deserializer is only called when the HTTP request is successful.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: 2b28499e-0a63-459a-accc-5ec509799756
* title: Consuming RESTful APIs (Part 1) 4
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

After the following code snippet runs, where can you find the resulting `Person` object?

```csharp
RestClient client = new RestClient();
RestRequest request = new RestRequest("http://localhost:3000/persons/99");
IRestResponse<Person> response = client.Get<Person>(request);
```

##### !end-question

##### !options

* `response.Data`
* `response.Content`
* `request.Data`
* `request.Content`


##### !end-options

##### !answer

* `response.Data`

##### !end-answer

##### !explanation-correct

`response.Content` contains the JSON string that was received and `response.Data` contains the deserialized `Person` object.

##### !end-explanation

##### !explanation:`response.Content`

`response.Content` contains the JSON string that was received, not a `Person` object.

##### !end-explanation

##### !explanation:`request.Data`

A `RestRequest` doesn't have a `Data` property.

##### !end-explanation

##### !explanation:`request.Content`

A `RestRequest` doesn't have a `Content` property.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: multiple-choice
* id: c6d09490-c52d-45b7-95e9-17e155728e71
* title: Consuming RESTful APIs (Part 1) 5
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

True or False: In order to deserialize JSON into an object, all the properties present in the JSON must also be present in the object.

##### !end-question

##### !options

* True
* False

##### !end-options

##### !answer

* False

##### !end-answer

##### !explanation-correct

Only properties with matching names have their values set. Other properties are ignored.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: multiple-choice
* id: c20408e3-cd83-4eb8-add6-b37097f4e9ff
* title: Consuming RESTful APIs (Part 1) 6
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

What's the output of this program if the JSON string returned by the API endpoint `http://localhost:3000/snacks/1` is `{"snack_name": "Potato Crisps", "vending_slot": "A1"}`?

```csharp
//---Snack.cs---
public class Snack
{
    public string VendingSlot { get; set; }
    public string Name { get; set; }
}

//---Program.cs---
public static void Main(string[] args)
{
    RestClient client = new RestClient();
    RestRequest request = new RestRequest("http://localhost:3000/snacks/1");
    IRestResponse<Snack> response = client.Get<Snack>(request);
    Snack snack = response.Data;
    Console.WriteLine("Snack: " + snack.Name);
    Console.WriteLine("Slot: " + snack.VendingSlot);
}
```

##### !end-question

##### !options

* Snack: Potato Crisps<br>Slot: A1
* Snack: Potato Crisps<br>Slot:
* Snack: <br>Slot: A1
* Snack: <br>Slot:

##### !end-options

##### !answer

* Snack: <br>Slot: A1

##### !end-answer

##### !explanation-correct

Since `snack_name` doesn't match `name` that property isn't set.

##### !end-explanation

##### !explanation:Snack: Potato Crisps<br>Slot: A1

That would be the output if both properties received values from the JSON, but one isn't being set.

##### !end-explanation

##### !explanation:Snack: Potato Crisps<br>Slot:

That output is missing the `VendingSlot` property which is set due to RestSharp matching it with `vending_slot`.

##### !end-explanation

##### !explanation:Snack: <br>Slot:

That would be the output if neither property received a value from the JSON, but one of them is being set.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: multiple-choice
* id: 6e493378-b2bf-4841-9da4-9d0e93feb3a1
* title: Consuming RESTful APIs (Part 1) 7
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

If this JSON was returned by the API endpoint referenced by `request`, which one of these lines of code would be correct?
```json
[
    {"snack_name": "Potato Crisps", "slot": "A1"},
    {"snack_name": "Stackers", "slot": "A2"},
    {"snack_name": "Grain Waves", "slot": "A3"}
]
```

##### !end-question

##### !options

* `IRestResponse<Snack> response = client.Get<Snack>(request);`
* `IRestResponse<List<Snack>> response = client.Get<Snack>(request);`
* `IRestResponse<Snack> response = client.Get<List<Snack>>(request);`
* `IRestResponse<List<Snack>> response = client.Get<List<Snack>>(request);`

##### !end-options

##### !answer

* `IRestResponse<List<Snack>> response = client.Get<List<Snack>>(request);`

##### !end-answer

##### !explanation-correct

The JSON contains an array, so the type parameter for both `Get<T>()` and `IRestResponse<T>` needs to be a collection like `List<Snack>`.

##### !end-explanation

##### !explanation:`IRestResponse<Snack> response = client.Get<Snack>(request);`

The JSON contains an array, but that line of code would try to make a single `Snack` object out of it.

##### !end-explanation

##### !explanation:`IRestResponse<List<Snack>> response = client.Get<Snack>(request);`

In that line of code `Get<Snack>()` would return a response object containing a single `Snack` object which doesn't match the type being assigned to which is a response object containing a `List` of `Snack` objects.

##### !end-explanation

##### !explanation:`IRestResponse<Snack> response = client.Get<List<Snack>>(request);`

In that line of code `Get<List<Snack>>()` would return a response object containing a list of `Snack` objects which doesn't match the type being assigned to which is a response object containing a single `Snack` object.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: ordering
* id: db9329ea-329b-43f0-b417-bf8d6825df55
* title: Consuming RESTful APIs (Part 1) 8
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

What order do the following events happen in when your code calls an API with the `Get<T>()` method of `RestClient`?

##### !end-question

##### !answer

1. An HTTP GET request is sent
1. An HTTP response is received
1. The deserializer creates a new object
1. The deserializer sets values for properties with matching names
1. `Get<T>()` returns an object implementing the `IRestResponse` interface

##### !end-answer

##### !explanation-incorrect

Hint: Think about the most fundamental thing you know about HTTP communication between a browser and a web server. After that, what happens to the JSON that's returned?

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
