# Unit summary

In this unit, you've learned how to:

- Make an HTTP `GET` request to a web API and process the response using RestSharp
- Parse the returned JSON using RestSharp and map to a C# object
