
# Handling errors

Eventually you'll encounter an error when interacting with an API. You might receive a 4xx or 5xx status code from the server, or your request may not reach the server due to network issues, such as losing connectivity or failed DNS lookup.

## Network errors

The `IRestResponse` object has a property called `ResponseStatus`. This property has several possible values, but the most commonly used ones are `ResponseStatus.Completed` and `ResponseStatus.Error`. This property is an `enum` like the `StatusCode` you learned about in the last unit.

If a response is received, regardless of the status code, the value is `ResponseStatus.Completed`. If a response isn't received, the value is `ResponseStatus.Error`.

There are other non-success conditions of `ResponseStatus`, so it's a good idea for your code to check if `ResponseStatus` doesn't equal `ResponseStatus.Completed`:

```csharp
if (response.ResponseStatus != ResponseStatus.Completed)
{
    // ...
}
```

When you have a situation where the response isn't received, there's a property of the `IRestResponse` object called `ErrorException`. This is a regular C# exception that contains information to help troubleshoot why a response wasn't received.

RestSharp is designed to not throw exceptions by default, but gives you the ability to throw one when it is appropriate.

The best practice in this situation is to throw an exception to the calling code, which needs be in a `try-catch` block itself to handle it. The following example is throwing a `new Exception` with `response.ErrorException` as the inner exception:

```csharp
// ApiService.cs
if (response.ResponseStatus != ResponseStatus.Completed)
{
    throw new Exception("Error occurred - unable to reach server.", response.ErrorException);
}

// Program.cs, calling code
try
{
    apiService.Get(); // this is the method that threw the exception
}
catch (Exception ex)
{
    // provide feedback to user that the request did not succeed
    Console.WriteLine(ex.Message); // prints "Error occurred - unable to reach server."
}
```

It's best to avoid displaying the raw exception information to the user as it probably isn't helpful to them. However, you can log the full exception to help you or other developers track down bugs.

The `ErrorException` is only used when a response isn't received by the server. HTTP errors—such as `404` or `500`—are handled differently as you'll see in the next section.

## Non-2xx response codes

As you learned in the previous unit, the RestSharp library provides a property to determine if a response was successful: `response.IsSuccessful`. This property is a boolean that returns `true` if the response status code is 200-299, so if you want to check for error conditions, you can use the `!` operator in front of `response.IsSuccessful`:

```csharp
if (!response.IsSuccessful)
{
    Console.WriteLine("Not a successful response");
}
```

### !callout-info

## Note: What about 1xx and 3xx response status codes?

Even though 1xx status codes are part of the HTTP specification, you won't often see one as a response from an API. 3xx codes are typically used by servers that host websites. For example, if a URL changed for a page, the server responds with a `301` or `302`. Web APIs typically don't change endpoints as it may break existing applications.

### !end-callout

You also saw in the last unit that if you need to handle a scenario for a specific status code, you can use a property called `response.StatusCode`. By default, this property returns the description of the status code, such as `OK` or `NotFound`. However, if you cast the property to an `int`, it returns the numerical status code:

```csharp
Console.WriteLine(response.StatusCode);
// Output: NotFound

Console.WriteLine((int)response.StatusCode);
// Output: 404
```

## Putting it together

Now you know how to handle different types of errors, how do you put these techniques together?

The following code is an example of the order to handle each condition, as well as when to handle a successful response:

```csharp
if (response.ResponseStatus != ResponseStatus.Completed)
{
    //response was not received, server was likely not reached
}
else if (!response.IsSuccessful)
{
    //response was received, but status code was not 2xx

    //if you want to handle specific status codes, you can, but it's not required
    if (response.StatusCode == HttpStatusCode.Unauthorized)  // 401
    {
        //handle specific status code of 401
    }
    else if ((int)response.StatusCode >= 500)  // Server errors
    {
        //handle 5xx status codes
    }
    else
    {
        //handle all other status codes
    }
}
else
{
    //successful response, 2xx
}
```
