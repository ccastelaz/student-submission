# Quiz
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: b0b0b5e8-5bcb-42f4-9c6b-8ee46e854dc5
* title: Consuming RESTful APIs (Part 2) 1
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

What `RestClient` method should you use to send a request to a web service that creates a new resource?

##### !end-question

##### !options

* `.Get()`
* `.Post()`
* `.Put()`
* `.AddJsonBody()`

##### !end-options

##### !answer

* `.Post()`

##### !end-answer

##### !explanation-correct

`.Post()` sends the request to the web service. Prior to calling `.Post()`, you use the `.AddJsonBody()` method of `RestRequest` to serialize the object and add it to the body of the request. 

##### !end-explanation

##### !explanation:`.Get()`

`.Get()` retrieves data.

##### !end-explanation

##### !explanation:`.AddJsonBody()`

`RestClient` does not have a `.AddJsonBody()` method. That method is part of `RestRequest`.

##### !end-explanation

##### !explanation:`.Put()`

The `.Put()` method is used for updating an existing resource, not creating a new one.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: 0edf4308-39cc-49e2-9b2e-7481c37556d3
* title: Consuming RESTful APIs (Part 2) 2
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

The following code is supposed to update `myPet` using a web service. What's the problem with it?

```csharp
myPet.FavoriteFood = "Tuna";
RestClient client = new RestClient(ApiUrl);
RestRequest request = new RestRequest("pets/" + myPet.Id);
request.AddJsonBody(myPet.FavoriteFood);
IRestResponse<Pet> response = client.Put<Pet>(request);
```

##### !end-question

##### !options

* passing `myPet.FavoriteFood` rather than `myPet` to `request.AddJsonBody()`
* appending the ID of `myPet` to the "pets/" API endpoint
* using `.Put<Pet>()` instead of `.Post<Pet>()`
* declaring `response` as a reference to an interface rather than a class 

##### !end-options

##### !answer

* passing `myPet.FavoriteFood` rather than `myPet` to `request.AddJsonBody()`


##### !end-answer

##### !explanation-correct

The entire object must be passed to `request.AddJsonBody()` because a `PUT` request typically replaces the entire object/record on the backend with the data sent.

##### !end-explanation

##### !explanation:appending the ID of `myPet` to the "pets/" API endpoint

Since the intent is to update an existing resource, the ID of that resource is normally included in the URL of the endpoint.

##### !end-explanation

##### !explanation:using `.Put<Pet>()` instead of `.Post<Pet>()`

`.Put<Pet>()` is the correct method of `RestTemplate` to use for updating an existing resource when you expect a serialized `Pet` object to be returned in the body of the response.

##### !end-explanation

##### !explanation:declaring `response` as a reference to an interface rather than a class

`IRestResponse<Pet>` is the type returned by `.Put<Pet>()`. The body of the response is deserialized into a `Pet` object and stored in the `Data` property.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: checkbox
* id: 33541625-a62a-46e4-9d98-64be9d31b0f6
* title: Consuming RESTful APIs (Part 2) 3
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

Which of the following problems can result in the `ResponseStatus` property of `IRestResponse` being something other than `ResponseStatus.Completed`?

##### !end-question

##### !options

* receiving a `403 Forbidden` response status code
* a poor wi-fi connection
* incorrect request headers
* the API server being down for maintenance
* incorrect spelling of the domain name in the address of the API endpoint

##### !end-options

##### !answer

* a poor wi-fi connection
* the API server being down for maintenance
* incorrect spelling of the domain name in the address of the API endpoint

##### !end-answer

##### !explanation-correct

Any problem that interferes with network communication and prevents receiving a response can result in the `ResponseStatus` property of `IRestResponse` being something other than `ResponseStatus.Completed`.

##### !end-explanation

##### !explanation:receiving a `403 Forbidden` response status code

If a response is received with a status code indicating an error, `ResponseStatus` is still `ResponseStatus.Completed`.

##### !end-explanation

##### !explanation:incorrect request headers

A problem with the request headers causes the API server to send a response with an error status code, and `ResponseStatus` is still `ResponseStatus.Completed`.

##### !end-explanation

##### !explanation-incorrect

You haven't yet selected all the problems in the list that can result in the `ResponseStatus` property of `IRestResponse` being something other than `ResponseStatus.Completed`.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: 9f979a7f-c811-4e08-b336-834c82072792
* title: Consuming RESTful APIs (Part 2) 4
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

True or False: Serialization is converting a string into an object.

##### !end-question

##### !options

* True
* False

##### !end-options

##### !answer

* False

##### !end-answer

##### !explanation-correct

Serialization is converting an object into a string.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: checkbox
* id: 3a02dff4-d13a-4af7-92f7-285a8b879ce3
* title: Consuming RESTful APIs (Part 2) 5
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

Which of these response status codes would cause the `IsSuccessful` property of `IRestResponse` to be false?

##### !end-question

##### !options

* `200 OK`
* `204 No Content`
* `404 Not Found`
* `451 Unavailable for Legal Reasons`
* `503 Service Unavailable`

##### !end-options

##### !answer

* `404 Not Found`
* `451 Unavailable for Legal Reasons`
* `503 Service Unavailable`

##### !end-answer

##### !explanation-correct

Any response status code in the 4xx or 5xx range causes `IsSuccessful` to be false.

##### !end-explanation

##### !explanation:`200 OK`

200 indicates success, so `IsSuccessful` is true.

##### !end-explanation

##### !explanation:`204 No Content`

204 indicates success, so `IsSuccessful` is true.

##### !end-explanation

##### !explanation-incorrect

You haven't yet selected all the response status codes that would cause `IsSuccessful` to be false.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: checkbox
* id: 14aa6f6e-995b-4825-8af2-1da986536748
* title: Consuming RESTful APIs (Part 2) 6
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

Which HTTP methods typically include a body with the request?

##### !end-question

##### !options

* `GET`
* `POST`
* `PUT`
* `DELETE`

##### !end-options

##### !answer

* `POST`
* `PUT`

##### !end-answer

##### !explanation-correct

`POST` and `PUT` add and update data. The data to be added or updated is sent in the body of the request.

##### !end-explanation

##### !explanation:`GET`
A `GET` request may include parameters, but doesn't include a body.
##### !end-explanation
##### !explanation:`DELETE`
A `DELETE` request doesn't include a body.
##### !end-explanation
##### !explanation-incorrect
You haven't yet selected all the methods that typically include a body.
##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: multiple-choice
* id: bbf21603-a9f5-443f-b653-0344ef8c67d8
* title: Consuming RESTful APIs (Part 2) 7
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

After sending a request with `RestClient` and receiving a response status code that indicates an error, which expression would give you the numeric value of that response status code? (Assume the `IRestResponse` is stored in the variable `response`.)

##### !end-question

##### !options

* `(int)response.StatusCode`
* `response.StatusCode`
* `(int)response.ResponseStatus`
* `response.ResponseStatus`

##### !end-options

##### !answer

* `(int)response.StatusCode`

##### !end-answer

##### !explanation-correct

The `StatusCode` property contains an enum which can be cast to an `int` to get the numeric value of the response status code.

##### !end-explanation

##### !explanation:`response.StatusCode`

The `StatusCode` property contains an enum, not a numeric value.

##### !end-explanation

##### !explanation:`(int)response.ResponseStatus`

The `ResponseStatus` property indicates if there were communication problems. It doesn't contain the response status code.

##### !end-explanation

##### !explanation:`response.ResponseStatus`

The `ResponseStatus` property indicates if there were communication problems. It doesn't contain the response status code.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: multiple-choice
* id: 83ae9dcf-caa6-40e9-bf94-d2f71d526328
* title: Consuming RESTful APIs (Part 2) 8
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

True or False: After creating the `RestRequest` for a `DELETE` request with the URL of the API endpoint, no further data needs to be added to it.

##### !end-question

##### !options

* True
* False

##### !end-options

##### !answer

* True

##### !end-answer

##### !explanation-correct

Typically, the URL includes the ID of the resource being deleted.

##### !end-explanation

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->