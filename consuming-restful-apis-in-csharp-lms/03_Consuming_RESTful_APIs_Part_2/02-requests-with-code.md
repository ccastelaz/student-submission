# Requests with code

As with `GET` requests, you can use RestSharp to perform `POST`, `PUT`, and `DELETE` requests. Similar to how you used `.Get()` for `GET` requests, there are corresponding `.Post()`, `.Put()`, and `.Delete()` methods.

To make a `POST` or `PUT` request, you'll often need to submit data as well. With RestSharp, you can submit that data as an object, and RestSharp automatically serializes it into a format that the server can read.

### !callout-info

## Note: What does serialize mean?

**Serialize** means converting an object into a string. It's the opposite of the term "deserialize", which you learned in the last unit.

### !end-callout

## POST requests

The syntax for making a `POST` request with an object is similar to the syntax for making a `GET` request with a type parameter. With `POST` requests, there's one additional step: attaching the data to the request. In the following example, the code creates a new `User` object and adds it to the request with `request.AddJsonBody(newUser)`:

```csharp
User newUser = new User
    { FirstName = "Fred", LastName = "Smith", Email = "fred@example.com" };

RestClient client = new RestClient(ApiUrl);
RestRequest request = new RestRequest("users");
request.AddJsonBody(newUser);
IRestResponse<User> response = client.Post<User>(request);
```

## PUT requests

A `PUT` request is like a `POST` request, but you use the `.Put()` method instead:

```csharp
User existingUser = new User
    { FirstName = "Fredrick", LastName = "Smith", Email = "fred@example.com" };

RestClient client = new RestClient(ApiUrl);
RestRequest request = new RestRequest("users/23");
request.AddJsonBody(existingUser);
IRestResponse<User> response = client.Put<User>(request);
```

Notice that the URL in this example has `23` at the end of it. A `PUT` method updates existing data, and typically a unique identifier, such as a numerical ID, is in the URL to identify the object that you want to update.

### PUT requests replace data

When performing a `PUT` request, you're usually expected to send the *entire object*, including parts that you haven't modified. This is because the server often *replaces* the entire object or record with the data supplied. This behavior is mainly due to the [HTTP standard](https://httpwg.org/specs/rfc7231.html#PUT), which states that a `PUT` request creates or replaces the existing data with the data that's sent.

It's up to the API developer to decide to follow the standard or not. As stated before, following the standard is best practice and indicates to other developers how the data changes on the server.

## DELETE requests

`DELETE` requests usually only require the ID, like `PUT` requests, but they don't require any additional data. You also don't need to use the type parameter as you won't usually receive any data from a `DELETE` request:

```csharp
RestClient client = new RestClient(ApiUrl);
RestRequest request = new RestRequest("users/23");
IRestResponse response = client.Delete(request);
```
