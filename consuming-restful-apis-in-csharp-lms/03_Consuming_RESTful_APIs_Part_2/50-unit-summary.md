# Unit summary

After reading this unit, you understand how to:

- Make `POST`, `PUT`, and `DELETE` requests using RestSharp
- Catch and handle errors using RestSharp
- Use Postman to make a `PUT`, `POST`, or `DELETE` request
