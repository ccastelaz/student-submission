
# Requests with Postman

You can use Postman to make `POST`, `PUT`, and `DELETE` requests in addition to making `GET` requests. You can change the request method by clicking on the drop-down to the left of the URL field:

![Postman methods drop-down](./img/postman-methods-1.png)

Select the request method that you want from the list:

![Postman methods list](./img/postman-methods-2.png)

### !callout-info

## Note: Postman request methods

When you open the drop-down list, you'll see that Postman offers other request methods. However, `GET`, `POST`, `PUT`, and `DELETE` are most commonly used with web APIs.

### !end-callout

The Intro to Tools: Postman unit can help you learn the Postman interface, how to create different types of requests, and how to add data to a request.

## POST requests

To send data for `POST` and `PUT` requests, you use the "Body" tab. To send data in JSON format, select the **raw** radio button. After you select the radio button, a drop-down appears to the right of the last radio button that allows you to select different "raw" formats, such as JSON:

![JSON format](./img/raw-json-format.png)

Once you send the request, if the API is configured to respond with data, you'll see the data in the response "Body" tab, similarly to `GET` requests:

![Postman POST Response](./img/postman-post-response.png)

You may notice that Postman also offers multiple body formats. The format you use depends on what the API expects and the data you're sending. The most commonly used formats are:

- `form-data` is the same way data is submitted on a website form using key-value pairs. You can also attach and upload files with this format.
- `x-www-form-urlencoded` is another key-value pair format, where all non-alphanumeric characters are replaced with a "URL encoded" value. You may be familiar with this if you've ever seen `%20` instead of a space in a filename. Files can't be attached in this format. This isn't usually interchangeable with `form-data`.
- `raw` lets you enter plain text. This format is helpful if you need to send JSON or XML.

## PUT requests

The same rules apply for `PUT` requests: you must send the entire data object or record, and you'll often use a numerical ID in the URL.

![Postman PUT Example](./img/postman-put-example.png)

## DELETE requests

As with making the request in code, a `DELETE` request doesn't typically need body data, and you'll often use a numerical ID in the URL as well:

![Postman DELETE Example](./img/postman-delete-example.png)
