#!/bin/bash

# Run the tests, sending output to a file
dotnet test > output.txt

# Search the results file for total number of tests.
# changed the lookbehind (see the Java exercises) to "reset match" (\K) because the lookbehind expression needs to be variable length, which is not allowed.
number_of_tests=$(grep -P -o "Total tests: *\K\d+" output.txt|head -1)

# The tests didn't run and something else is wrong
if [ -z $number_of_tests ]
then
    # Java tests used 'sed' to print a portion of the file. Here we are printing the entire output file
    echo 'There was an error that prevented the tests from executing:'
    echo
    cat output.txt
    exit 1  ### Error
fi

# changed the lookbehind to "reset match" (\K) because the lookbehind expression needs to be variable length, which is not allowed.
number_failed=$(grep -P -o "Failed: *\K\d+" output.txt|head -1)
number_passed=$(grep -P -o "Passed: *\K\d+" output.txt|head -1)
# number_skipped=$(grep -P -o "Skipped: *\K\d+" output.txt|head -1)

if [ -z "$number_passed" ]
then
    number_passed=0 # "Passed" line doesn't appear if there's no passing tests, so must set to 0 for (number_passed of number_of_tests)
fi

# echo Failed: $number_failed
# echo Passed: $number_passed
# echo Skipped: $number_skipped

# Calculate the pass rate
percent_passed=$(((number_passed)*100 / number_of_tests))
echo --- ---------------------------------------------------------------------------
echo --- $percent_passed% tests passing \($number_passed of $number_of_tests\).
echo --- ---------------------------------------------------------------------------
echo

# Print the portion of the output file that follows the search phrase
sed -n '/Starting test execution, please wait.../,$p' output.txt


if [ $percent_passed -ge 50 ]
then
    # return 0 for pass
    exit 0
else
    # return non-zero for fail
    exit 1
fi
